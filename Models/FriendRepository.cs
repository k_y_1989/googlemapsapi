﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServerGoogleAPI.Models
{
    public class FriendRepository : IFriendRepository
    {
        public IEnumerable<Friend> GetAll(int idUser)
        {
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            return context.Friends.Where(p => p.IDUser == idUser).ToList();
        }

        public Friend Get(int idUser, int idFriend)
        {
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            var item = context.Friends.Where(p => p.IDUser == idUser && p.IDUserFriend == idFriend).Single();
            return (Friend)item;
        }

        public Friend Add(Friend f)
        {
            if (f == null)
            {
                throw new ArgumentNullException("item");
            }
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            context.Friends.Add(f);            
            context.SaveChanges();
            return f;
        }

        
        public void Remove(int idUser, int idFriend)
        {
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled =false;
            var idF = context.Friends.Where(p => p.IDUserFriend == idFriend && p.IDUser == idUser).ToList().FirstOrDefault();
            if (idF.IDUserFriend.ToString() != "")
            {
                context.Friends.Remove(idF);
                context.SaveChanges();
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public bool Update(Friend f)
        {
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            if (f == null)
            {
                throw new ArgumentNullException("item");
            }
            Friend fUpdate = context.Friends.Where(p => p.IDUser == f.IDUser && p.IDUserFriend == f.IDUserFriend).Single();
            if (fUpdate == null)
                return false;
            fUpdate = f;
            context.SaveChanges();
            return true;
        }
        public IEnumerable<Friend> GetFriendOnline(int idUser, Boolean Status)
        {
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            return context.Friends.Where(p => p.IDUser == idUser && p.TinhTrang == Status).ToList();
        }
    }
}