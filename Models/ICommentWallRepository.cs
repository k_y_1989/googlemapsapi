﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServerGoogleAPI.Models
{
    public interface ICommentWallRepository
    {
        IEnumerable<CommentWall> GetAll(int idW);
        CommentWall Get(int id);
        CommentWall Add(CommentWall cw);
        void Remove(int id);
        bool Update(CommentWall cw);
    }
}