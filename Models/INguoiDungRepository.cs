﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServerGoogleAPI.Models
{
    public interface INguoiDungRepository
    {
        IEnumerable<NguoiDung> GetAll();
        NguoiDung Get(int id);
        NguoiDung Get(string tenDangNhap, string matKhau);
        NguoiDung Add(NguoiDung nd);
        void Remove(int id);
        bool Update(NguoiDung nd);
    }
}