﻿create database GoogleMapsAPI
go 
use GoogleMapsAPI
go

create table NguoiDung
(
	ID int not null IDENTITY(1,1), 
	TenDangNhap varchar(30) not null,
	MatKhau varchar(20) not null,
	HoTen nvarchar(50) not null,
	NgaySinh datetime,
	HinhAnh nvarchar(100),
	GioiTinh nvarchar(10),
	Email nvarchar(100),
	Phone varchar(15),
	SoThich nvarchar(200),
	TinhTrang bit,
	KinhDo float not null,
	ViDo float not null,
	PRIMARY KEY(ID)
)

insert into NguoiDung(TenDangNhap, MatKhau, HoTen, NgaySinh, HinhAnh, GioiTinh, Email, Phone, SoThich, TinhTrang, KinhDo, ViDo) 
values('NamDao', '123456', N'Nam Đào', '12/12/1990', N'', N'Nam', 'dnhnam.khtn@gmail.com', '0912345678', N'Ăn nhậu', 1, 123.5, 135.5)
insert into NguoiDung(TenDangNhap, MatKhau, HoTen, NgaySinh, HinhAnh, GioiTinh, Email, Phone, SoThich, TinhTrang, KinhDo, ViDo) 
values('k_y_1989', '123456', N'Điền Huỳnh Phong', '07/20/1989', N'', N'Nam', 'dhphong@selab.hcmus.edu.com', '0989721945', N'Truyện, chơi game, nghe nhạc', 1, 100.5, 111.5)
insert into NguoiDung(TenDangNhap, MatKhau, HoTen, NgaySinh, HinhAnh, GioiTinh, Email, Phone, SoThich, TinhTrang, KinhDo, ViDo) 
values('user', '123456', N'User', '07/20/1989', N'',N'Nam', 'user@gmail.com', '0900000000', N'Du lịch', 0, 200.5, 191.5)

create table Friend
(
	IDUser int not null,
	IDUserFriend int not null,
	TinhTrang bit,
	KetBan bit,
	PRIMARY KEY(IDUser, IDUserFriend)
)

ALTER TABLE Friend
ADD CONSTRAINT FK_Friend_NguoiDung
FOREIGN KEY(IDUser)
REFERENCES NguoiDung(ID)

ALTER TABLE Friend
ADD CONSTRAINT FK_Friend_NguoiDungBan
FOREIGN KEY(IDUserFriend)
REFERENCES NguoiDung(ID)

insert into Friend(IDUser, IDUserFriend, TinhTrang, KetBan) 
values(1, 2, 0, 1)
insert into Friend(IDUser, IDUserFriend, TinhTrang, KetBan)  
values(1, 3, 0, 1)
insert into Friend(IDUser, IDUserFriend, TinhTrang, KetBan)  
values(2, 1, 0, 1)
insert into Friend(IDUser, IDUserFriend, TinhTrang, KetBan)  
values(2, 3, 0, 1)
insert into Friend(IDUser, IDUserFriend, TinhTrang, KetBan)  
values(3, 1, 0, 1)
insert into Friend(IDUser, IDUserFriend, TinhTrang, KetBan)  
values(3, 2, 0, 1)

create table Wall
(
	ID bigint not null IDENTITY(1,1),
	IDNguoiDung int not null,
	IDNguoiPost int not null,
	Thich int,
	KhongTich int,
	NoiDungComment nvarchar(200),
	NgayPost datetime,
	PRIMARY KEY(ID)
)

ALTER TABLE Wall
ADD CONSTRAINT FK_Wall_NguoiDung
FOREIGN KEY(IDNguoiDung)
REFERENCES NguoiDung(ID)

ALTER TABLE Wall
ADD CONSTRAINT FK_Wall_NguoiPost
FOREIGN KEY(IDNguoiPost)
REFERENCES NguoiDung(ID)

create table CommentWall
(
	ID bigint not null IDENTITY(1,1),
	IDWall bigint not null,
	IDNguoiDungComment int not null,
	NoiDungComment nvarchar(200),
	Thich int,
	KhongTich int,
	PRIMARY KEY(ID)
)

ALTER TABLE CommentWall
ADD CONSTRAINT FK_CommentWall_Wall
FOREIGN KEY(IDWall)
REFERENCES Wall(ID)

ALTER TABLE CommentWall
ADD CONSTRAINT FK_CommentWall_NguoiDungComment
FOREIGN KEY(IDNguoiDungComment)
REFERENCES NguoiDung(ID)