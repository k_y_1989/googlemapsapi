﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebServerGoogleAPI.Models;

namespace WebServerGoogleAPI.Controllers
{
    public class FriendController : ApiController
    {
        // GET api/friend
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/friend/5
        public HttpResponseMessage Get(int idUser, int idFriend)
        {
            Friend f = new FriendRepository().Get(idUser, idFriend);
            if (f == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Friend Not found for the Given ID");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, f);
            }
        }
        public HttpResponseMessage GetFriendOnline(int idUser, Boolean Status)
        {
            IEnumerable<Friend> listOnline = new FriendRepository().GetFriendOnline(idUser, Status);
            if (listOnline == null)
            {
                 return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Friend Not found for the Given ID");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, listOnline);            
            }

        }
        // POST api/friend
        public HttpResponseMessage PostFriend(Friend fr)
        {
            //dynamic data = nd ;
            fr = new FriendRepository().Add(fr);
            //NguoiDung aa = new NguoiDungRepository().Get((int)data.id);
            var response = Request.CreateResponse<Friend>(HttpStatusCode.Created, fr);
            string uri = Url.Link("DefaultApi", new { id = fr.IDUser });
            response.Headers.Location = new Uri(uri);
            return response;
        }

       

        // PUT api/friend/5
        public void Put(int id, [FromBody]string value)
        {
        }
        public HttpResponseMessage PutFriend(Friend fr)
        {
            
            if (!new FriendRepository().Update(fr))
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Unable to Update the Nguoi Dung for the Given ID");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }
        
        // DELETE api/friend/5
        public HttpResponseMessage DeleteFriend(int idFriend, int idUser)
        {
            new FriendRepository().Remove(idUser, idFriend);
            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }
    }
}
