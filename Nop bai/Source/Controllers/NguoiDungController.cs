﻿
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebServerGoogleAPI.Models;

namespace WebServerGoogleAPI.Controllers
{
    public class NguoiDungController : ApiController
    {
        //
        // GET: /NguoiDung/
        public IEnumerable<NguoiDung> GetAll()
        {
            return new NguoiDungRepository().GetAll();
        }

        //lay thong tin nguoi dung
        public HttpResponseMessage GetNguoiDung(int id)
        {
            NguoiDung nd = new NguoiDungRepository().Get(id);
            if (nd == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Nguoi Dung Not found for the Given ID");
            }

            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, nd);
            }
        }

        //dang nhap
        public HttpResponseMessage GetNguoiDung(string tendangnhap, string matkhau)
        {
            NguoiDung nd = new NguoiDungRepository().Get(tendangnhap, matkhau);
            if (nd == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Nguoi Dung Not found");
            }

            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, nd);
            }
        }

        public HttpResponseMessage PostNguoiDung(NguoiDung nd)
        {
            //dynamic data = nd ;
            nd = new NguoiDungRepository().Add(nd);
            //NguoiDung aa = new NguoiDungRepository().Get((int)data.id);
            var response = Request.CreateResponse<NguoiDung>(HttpStatusCode.Created, nd);
            string uri = Url.Link("DefaultApi", new { id = nd.ID });
            response.Headers.Location = new Uri(uri);
            return response;
        }

        public HttpResponseMessage PutNguoiDung(int id, NguoiDung nd)
        {
            nd.ID = id;
            if (!new NguoiDungRepository().Update(nd))
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Unable to Update the Nguoi Dung for the Given ID");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        public HttpResponseMessage DeleteNguoiDung(int id)
        {
            new NguoiDungRepository().Remove(id);
            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }
        
    }
}
