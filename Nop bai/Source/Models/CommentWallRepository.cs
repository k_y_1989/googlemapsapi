﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServerGoogleAPI.Models
{
    public class CommentWallRepository:ICommentWallRepository
    {

        public IEnumerable<CommentWall> GetAll(int idW)
        {
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            return context.CommentWalls.Where(p=>p.IDWall == idW).ToList();
        }

        public CommentWall Get(int id)
        {
            throw new NotImplementedException();
        }

        public CommentWall Add(CommentWall cw)
        {
            if (cw == null)
            {
                throw new ArgumentNullException("item");
            }
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            context.CommentWalls.Add(cw);
            context.SaveChanges();
            return cw;
        }

        public void Remove(int id)
        {
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            CommentWall cw = context.CommentWalls.Where(p => p.ID == id).Single();
            if (cw == null)
            {
                throw new ArgumentNullException("item");
            }
            context.CommentWalls.Remove(cw);
            context.SaveChanges();
        }

        public bool Update(CommentWall cw)
        {
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            if (cw == null)
            {
                throw new ArgumentNullException("item");
            }
            CommentWall cwUpdate = context.CommentWalls.Where(p => p.ID == cw.ID).Single();
            if (cwUpdate == null)
                return false;
            cwUpdate = cw;
            context.SaveChanges();
            return true;
        }
    }
}