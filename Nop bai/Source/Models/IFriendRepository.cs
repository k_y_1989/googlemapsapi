﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServerGoogleAPI.Models
{
    public interface IFriendRepository
    {
        IEnumerable<Friend> GetAll(int idUser);
        Friend Get(int idUser, int idFriend);
        Friend Add(Friend f);
        void Remove(int idUser, int idFriend);
        bool Update(Friend f);
        IEnumerable<Friend> GetFriendOnline(int idUser, Boolean Status);
    }
}