﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Thinktecture.IdentityModel.Http.Cors.WebApi;

namespace WebServerGoogleAPI.App_Start
{
    public class CorsConfig
    {
        public static void RegisterCors(HttpConfiguration httpConfig)
        {
            WebApiCorsConfiguration corsConfig = new WebApiCorsConfiguration();

            // this adds the CorsMessageHandler to the HttpConfiguration’s
            // MessageHandlers collection
            corsConfig.RegisterGlobal(httpConfig);

            // this allow all CORS requests to the Products controller
            // from the http://foo.com origin.
            corsConfig
                .ForResources("NguoiDung")
                .ForOrigins("http://localhost:37151")
                .AllowAll();

            corsConfig
                .ForResources("Friend")
                .ForOrigins("http://localhost:37151")
                .AllowAll();

            corsConfig
                .ForResources("Wall")
                .ForOrigins("http://localhost:37151")
                .AllowAll();

            corsConfig
                .ForResources("CommentWall")
                .ForOrigins("http://localhost:37151")
                .AllowAll();
        }
    }
}