﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebServerGoogleAPI.Models;

namespace WebServerGoogleAPI.Controllers
{
    public class FriendController : ApiController
    {
        // GET api/friend
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/friend/5
        public HttpResponseMessage Get(int idUser)
        {
            IEnumerable<NguoiDung> f = new FriendRepository().GetAll(idUser);
            if (f == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Friend Not found for the Given ID");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, f);
            }
        }

        //lay danh sach yeu cau ket ban
        public HttpResponseMessage Get(int idUser, bool ketban)
        {
            IEnumerable<NguoiDung> f = new FriendRepository().GetAll(idUser, ketban);
            if (f == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Friend Not found for the Given ID");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, f);
            }
        }

        public HttpResponseMessage Get(int idUser, int idFriend)
        {
            Friend f = new FriendRepository().GetAll(idUser, idFriend);
            if (f == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Friend Not found for the Given ID");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, f);
            }
        }

        //public HttpResponseMessage Get(int idUser, int idFriend)
        //{
        //    IEnumerable<NguoiDung> f = new FriendRepository().GetAll(idUser);
        //    if (f == null)
        //    {
        //        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Friend Not found for the Given ID");
        //    }
        //    else
        //    {
        //        return Request.CreateResponse(HttpStatusCode.OK, f);
        //    }
        //}
       

        // POST api/friend
        
        public HttpResponseMessage PostFriend(Friend fri)
        {
            //dynamic data = nd ;
            fri = new FriendRepository().Add(fri);
            //NguoiDung aa = new NguoiDungRepository().Get((int)data.id);
            var response = Request.CreateResponse<Friend>(HttpStatusCode.Created, fri);
            string uri = Url.Link("DefaultApi", new { id = fri.IDUser});
            response.Headers.Location = new Uri(uri);
            return response;
        }


        // PUT api/friend/5
        public HttpResponseMessage PutFriend(Friend fr)
        {
            if (!new FriendRepository().Update(fr))
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Unable to Update the Nguoi Dung for the Given ID");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        // DELETE api/friend/5
        public void Delete(int id)
        {
        }
    }
}
