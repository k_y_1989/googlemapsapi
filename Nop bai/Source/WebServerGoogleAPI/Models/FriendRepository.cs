﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServerGoogleAPI.Models
{
    public class FriendRepository : IFriendRepository
    {
        //lay danh sach ban be
        public IEnumerable<NguoiDung> GetAll(int idUser)
        {
            List<NguoiDung> listnd = new List<NguoiDung>();
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            var listId = context.Friends.Where(p => p.IDUser == idUser && p.TinhTrang==false && p.KetBan == true).ToList();
            foreach (var nd in listId)
            {
                NguoiDung ndung = context.NguoiDungs.Where(p => p.ID == nd.IDUserFriend).Single();
                listnd.Add(ndung);
            }
            return listnd;
        }

        //lay danh sach yeu cau ket ban
        public IEnumerable<NguoiDung> GetAll(int idUser, bool ketban)
        {
            List<NguoiDung> listnd = new List<NguoiDung>();
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            var listId = context.Friends.Where(p => p.IDUser == idUser && p.KetBan == false).ToList();
            foreach (var nd in listId)
            {
                NguoiDung ndung = context.NguoiDungs.Where(p => p.ID == nd.IDUserFriend).Single();
                listnd.Add(ndung);
            }
            return listnd;
        }

        public Friend GetAll(int idUser, int idFriend)
        {
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            return context.Friends.Where(p => p.IDUser == idUser && p.IDUserFriend == idFriend).Single();
        }

        //Tim Ban
        public bool TimBan(int idUser, int idFriend)
        {
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            try
            {
                if (context.Friends.Where(p => p.IDUser == idUser && p.IDUserFriend == idFriend).FirstOrDefault() != null ||
                    context.Friends.Where(p => p.IDUserFriend == idUser && p.IDUser == idFriend).FirstOrDefault() != null)
                    return true;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }            
            return false;
        }


        public Friend Add(Friend f)
        {
            if (f == null)
            {
                throw new ArgumentNullException("item");
            }
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            context.Friends.Add(f);            
            context.SaveChanges();
            return f;
        }

        
        public Boolean Remove(int idUser, int idFriend)
        {
            Boolean flag = false;
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled =false;
            var idF = context.Friends.Where(p => p.IDUserFriend == idFriend && p.IDUser == idUser).ToList().FirstOrDefault();
            if (idF.IDUserFriend.ToString() != "")
            {
                context.Friends.Remove(idF);
                context.SaveChanges();
                flag = true;
            }
            else
            {
                throw new NotImplementedException();
            }
            return flag;            
        }

        public bool Update(Friend f)
        {
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            if (f == null)
            {
                throw new ArgumentNullException("item");
            }
           // Friend fUpdate = context.Friends.Where(p => p.IDUser == f.IDUser && p.IDUserFriend == f.IDUserFriend).Single();
            try
            {
                var item = context.Friends.Where(p => p.IDUser == f.IDUser && p.IDUserFriend == f.IDUserFriend).Single();
                if (item != null)
                {
                    Friend fri = (Friend)item;
                    fri.KetBan = f.KetBan;
                    fri.TinhTrang = f.TinhTrang;
                    context.SaveChanges();

                    //Friend ff = new Friend();
                    //ff.IDUser = fri.IDUserFriend;
                    //ff.IDUserFriend = fri.IDUser;
                    //ff.TinhTrang = false;
                    //ff.KetBan = true;
                    //Add(ff);
                }
            }
            catch
            {
                return false;
            }
            return true;            
        }
    }
}