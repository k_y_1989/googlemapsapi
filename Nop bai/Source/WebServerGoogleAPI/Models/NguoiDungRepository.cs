﻿using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Web;

namespace WebServerGoogleAPI.Models
{
    public class NguoiDungRepository:INguoiDungRepository
    {

        public IEnumerable<NguoiDung> GetAll()
        {
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            return context.NguoiDungs.ToList();
        }

        public NguoiDung Get(int id)
        {
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            var item = context.NguoiDungs.Where(p=>p.ID == id).Single();
            return (NguoiDung)item;
        }

        public NguoiDung Get(string tenDangNhap, string matKhau)
        {
            NguoiDung nd = null;
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            try
            {
                var item = context.NguoiDungs.Where(p => p.TenDangNhap == tenDangNhap && p.MatKhau == matKhau).Single();
                if (item != null)
                {
                    nd = (NguoiDung)item;
                    nd.TinhTrang = true;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                nd = null;
            }
            
            return nd;
        }

        //tim bạn
        public IEnumerable<NguoiDung> Get(string tenDangNhap, int ID)
        {
            List<NguoiDung> listnd = new List<NguoiDung>();
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            var list = context.NguoiDungs.Where(p => p.TenDangNhap.CompareTo(tenDangNhap)>=1 && p.ID!=ID).ToList();
            foreach (var item in list)
            {
                if (new FriendRepository().TimBan(ID, item.ID) == false)
                    listnd.Add(item);
            }
            return listnd;
        }

        public NguoiDung Add(NguoiDung nd)
        {
            if (nd == null)
            {
                throw new ArgumentNullException("item");
            }
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            context.NguoiDungs.Add(nd);
            context.SaveChanges();
            return nd;
        }

        public void Remove(int id)
        {
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            NguoiDung nd = context.NguoiDungs.Where(p=>p.ID == id).Single();
            if (nd == null)
            {
                throw new ArgumentNullException("item");
            }
            context.NguoiDungs.Remove(nd);
            context.SaveChanges();
        }

        public bool Update(NguoiDung nd)
        {
            
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            if (nd == null)
            {
                throw new ArgumentNullException("item");
            }
            try
            {
                var item = context.NguoiDungs.Where(p => p.ID == nd.ID).Single();
                if (item != null)
                {
                    NguoiDung user = (NguoiDung)item;
                    user.MatKhau = nd.MatKhau;
                    user.KinhDo = nd.KinhDo;
                    user.ViDo = nd.ViDo;
                    user.TinhTrang = nd.TinhTrang;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
            return true;
        }
    }
}