﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebServerGoogleAPI.Models;
namespace WebServerGoogleAPI.Controllers
{
    public class CommentWallController : ApiController
    {
        // GET api/commentwall
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/commentwall/5
        public string Get(int id)
        {
            return "value";
        }
        public HttpResponseMessage GetCommentWall(int idWall)
        {
            IEnumerable<CommentWall> cw = new CommentWallRepository().GetAll(idWall);
            if (cw == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Comment wall Not found for the Given ID");
            }

            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, cw);
            }
        }
        // POST api/commentwall
        public void Post([FromBody]string value)
        {
        }

        // PUT api/commentwall/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/commentwall/5
        public void Delete(int id)
        {
        }
    }
}
