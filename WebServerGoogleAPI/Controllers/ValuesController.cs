﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebServerGoogleAPI.Models;

namespace WebServerGoogleAPI.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<NguoiDung> GetDanhSachNguoiDung()
        {
            return new NguoiDungRepository().GetAll();
        }

        // GET api/values/5
        public NguoiDung Get(int id)
        {
            return new NguoiDungRepository().Get(id);
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}