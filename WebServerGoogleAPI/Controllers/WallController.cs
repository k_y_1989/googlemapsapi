﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebServerGoogleAPI.Models;

namespace WebServerGoogleAPI.Controllers
{
    public class WallController : ApiController
    {
        // GET api/wall
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/wall/5
        public HttpResponseMessage GetWall(int idUser)
        {
            IEnumerable<Wall> w = new WallRepository().GetAll(idUser);
            if (w == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Wall Not found for the Given ID");
            }

            else
            {
                return Request.CreateResponse(HttpStatusCode.OK,w);
            }
        }
        // POST api/wall
       public HttpResponseMessage PostWall(Wall w)
        {
            w = new WallRepository().Add(w);
            var response = Request.CreateResponse<Wall>(HttpStatusCode.Created, w);
            string uri = Url.Link("DefaultApi", new { id = w.ID });
            response.Headers.Location = new Uri(uri);
            return response;
        }

        // PUT api/wall/5
        public HttpResponseMessage PutTotalLike(int idwall)
        {
            int like = new WallRepository().TotalLikewall(idwall);
            if (like ==null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Unable to Update the wall for the Given ID");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }
        public HttpResponseMessage PutTotalUnLike(int idwall)
        {
            int unlike = new WallRepository().TotalUnLikewall(idwall);
            if (unlike ==null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Unable to Update the wall for the Given ID");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        // DELETE api/wall/5
        public HttpResponseMessage DeleteWall(int id)
        {
            new WallRepository().Remove(id);
            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }
    }
}
