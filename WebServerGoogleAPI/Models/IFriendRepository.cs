﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServerGoogleAPI.Models
{
    public interface IFriendRepository
    {
        IEnumerable<NguoiDung> GetAll(int idUser);
        IEnumerable<NguoiDung> GetAll(int idUser, bool ketban);
        Friend GetAll(int idUser, int idFriend);
        Friend Add(Friend f);
        Boolean Remove(int idUser, int idFriend);
        bool Update(Friend f);
    }
}