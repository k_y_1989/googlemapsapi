﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServerGoogleAPI.Models
{
    public interface IWallRepository
    {
        IEnumerable<Wall> GetAll(int idUser);
        Wall Get(int id);
        Wall Add(Wall w);
        void Remove(int id);
        bool Update(Wall w);
        int TotalLikewall(int iduser);
    }
}