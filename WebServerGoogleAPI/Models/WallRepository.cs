﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServerGoogleAPI.Models
{
    public class WallRepository : IWallRepository
    {
        public IEnumerable<Wall> GetAll(int idUser)
        {
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            //sap xep giam dan theo ngay
            return context.Walls.Where(p => p.IDNguoiDung == idUser).OrderByDescending(p => p.NgayPost).ToList();
        }

        public Wall Get(int id)
        {
            throw new NotImplementedException();
        }
        public int TotalLikewall(int idwall)
        {
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            var likewall = context.Walls.Where(p => p.ID == idwall).SingleOrDefault();
            int demlike = likewall.Thich.Value;
            demlike += 1;
            return demlike;
               
        }
        public int TotalUnLikewall(long idwall)
        {
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            var unlikewall = context.Walls.Where(p => p.ID == idwall).SingleOrDefault();
            int demunlike = unlikewall.KhongTich.Value;
            demunlike += 1;
            return demunlike;

        }
        public Wall Add(Wall w)
        {
            if (w == null)
            {
                throw new ArgumentNullException("item");
            }
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            context.Walls.Add(w);
            context.SaveChanges();
            return w;
        }

        public void Remove(int id)
        {
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            Wall w = context.Walls.Where(p => p.ID == id).Single();
            if (w == null)
            {
                throw new ArgumentNullException("item");
            }
            context.Walls.Remove(w);
            context.SaveChanges();
        }

        //post them mot Wall với tiêu đề
        public bool Update(Wall w)
        {
            GoogleMapsAPIEntities context = new GoogleMapsAPIEntities();
            context.Configuration.ProxyCreationEnabled = false;
            if (w == null)
            {
                throw new ArgumentNullException("item");
            }
            Wall wUpdate = context.Walls.Where(p => p.ID == w.ID).Single();
            if (wUpdate == null)
                return false;
            wUpdate = w;
            context.SaveChanges();
            return true;
        }
    }
}